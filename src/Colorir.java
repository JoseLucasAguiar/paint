
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Lucas
 */
public class Colorir {
    
    
    
    public void colorir(BufferedImage buffer, Point inicio, Color corInicio, Color novaCor){
        
        Stack<Point> pontos = new Stack();
        //List<Point> pontos = new ArrayList<>();
        //pontos.add(inicio);
        pontos.push(inicio);
        
        if(corInicio != novaCor)
        {
            while(!pontos.isEmpty()){
                //Point p = pontos.remove(0);
                Point p = pontos.pop();
                
                if( !(p.getY() + 1 >= buffer.getHeight())){
                    Color top = new Color(buffer.getRGB(p.getX(), p.getY() + 1), true);

                    if(top.equals(corInicio)){
                        //pontos.add(new Point(p.getX(), p.getY() + 1,  0, top));
                        pontos.push(new Point(p.getX(), p.getY() + 1,  0, top));
                        buffer.setRGB(p.getX(), p.getY() + 1, novaCor.getRGB());
                    }
                }
                if( !(p.getX() + 1 >= buffer.getWidth())){
                    Color rigth = new Color(buffer.getRGB(p.getX() + 1, p.getY()), true);

                    if(rigth.equals(corInicio)){
                        //pontos.add(new Point(p.getX() + 1, p.getY(),  0, novaCor));
                        pontos.push(new Point(p.getX() + 1, p.getY(),  0, novaCor));
                        buffer.setRGB(p.getX() + 1, p.getY(), novaCor.getRGB());
                    }
                }
                if(!(p.getY() - 1 < 0)){
                    Color bottom = new Color(buffer.getRGB(p.getX(), p.getY() - 1), true);

                    if(bottom.equals(corInicio)){
                        //pontos.add(new Point(p.getX(), p.getY() - 1,  0, corInicio));
                        pontos.push(new Point(p.getX(), p.getY() - 1,  0, corInicio));
                        buffer.setRGB(p.getX(), p.getY() - 1, novaCor.getRGB());
                    }
                }
                if( !(p.getX() - 1 < 0)){
                    Color left = new Color(buffer.getRGB(p.getX() - 1, p.getY()), true);

                    if(left.equals(corInicio)){
                        //pontos.add(new Point(p.getX() - 1, p.getY(),  0, corInicio));
                        pontos.push(new Point(p.getX() - 1, p.getY(),  0, corInicio));
                        buffer.setRGB(p.getX() - 1, p.getY(), novaCor.getRGB());
                    }
                }
            }
        }
    }
        
    public void escrever(Graphics buffer, Point ponto, String msg, Color cor){
        buffer.setColor(cor);
        buffer.setFont(new Font("TimesRoman", Font.PLAIN, 25));
        buffer.drawString(msg, ponto.getX(), ponto.getY());
    }
    
    public void salvarImagem(BufferedImage buffer){
        
    }
}
