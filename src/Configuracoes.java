
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JColorChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Jose Lucas
 */
public class Configuracoes extends javax.swing.JFrame {

    public static Color COR = Color.BLACK;
    public static int width;
    public static Opcao OPCAO = Opcao.DEFAULT;
    public static int TAMANHO = 30;
    
    public Configuracoes() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double height = screenSize.getHeight();
        this.setResizable(false);
        initComponents();
        width = (int) this.getWidth();
        this.setSize(this.getWidth(), (int)height);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoOpcoes = new javax.swing.ButtonGroup();
        btnColorChooser = new javax.swing.JButton();
        rdBrush = new javax.swing.JRadioButton();
        rdBorracha = new javax.swing.JRadioButton();
        txtTamanhoNovo = new javax.swing.JTextField();
        rdEscrever = new javax.swing.JRadioButton();
        rdColorir = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        btnAlteraTamanho = new javax.swing.JButton();
        btnSalvaImg = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setResizable(false);

        btnColorChooser.setText("Mudar a cor");
        btnColorChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnColorChooserActionPerformed(evt);
            }
        });

        grupoOpcoes.add(rdBrush);
        rdBrush.setText("Brush");
        rdBrush.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdBrushActionPerformed(evt);
            }
        });

        grupoOpcoes.add(rdBorracha);
        rdBorracha.setText("Borracha");
        rdBorracha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdBorrachaActionPerformed(evt);
            }
        });

        grupoOpcoes.add(rdEscrever);
        rdEscrever.setText("Escrever");
        rdEscrever.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdEscreverActionPerformed(evt);
            }
        });

        grupoOpcoes.add(rdColorir);
        rdColorir.setText("Colorir");
        rdColorir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdColorirActionPerformed(evt);
            }
        });

        jLabel1.setText("Tamanho");

        btnAlteraTamanho.setText("Alt.Tamanho");
        btnAlteraTamanho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlteraTamanhoActionPerformed(evt);
            }
        });

        btnSalvaImg.setText("Salvar");
        btnSalvaImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvaImgActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnColorChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTamanhoNovo)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rdBorracha)
                                    .addComponent(rdBrush)
                                    .addComponent(rdEscrever)
                                    .addComponent(rdColorir)
                                    .addComponent(jLabel1))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(btnAlteraTamanho, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSalvaImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(btnColorChooser)
                .addGap(18, 18, 18)
                .addComponent(rdBrush)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rdColorir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rdBorracha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rdEscrever)
                .addGap(25, 25, 25)
                .addComponent(jLabel1)
                .addGap(7, 7, 7)
                .addComponent(txtTamanhoNovo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAlteraTamanho)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvaImg)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        setBounds(0, 0, 117, 361);
    }// </editor-fold>//GEN-END:initComponents

    private void btnColorChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnColorChooserActionPerformed
        COR = JColorChooser.showDialog(
                     this,
                     "Escolha a cor desejada",
                     Color.red);
    }//GEN-LAST:event_btnColorChooserActionPerformed

    private void rdBrushActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdBrushActionPerformed
        OPCAO = Opcao.BRUSH;
    }//GEN-LAST:event_rdBrushActionPerformed

    private void rdBorrachaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdBorrachaActionPerformed
        OPCAO = Opcao.BORRACHA;
    }//GEN-LAST:event_rdBorrachaActionPerformed

    private void rdEscreverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdEscreverActionPerformed
        OPCAO = Opcao.ESCREVER;
    }//GEN-LAST:event_rdEscreverActionPerformed

    private void rdColorirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdColorirActionPerformed
        OPCAO = Opcao.COLORIR;
    }//GEN-LAST:event_rdColorirActionPerformed

    private void btnAlteraTamanhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlteraTamanhoActionPerformed
        try{
            TAMANHO = Integer.parseInt(txtTamanhoNovo.getText());
        }catch(NumberFormatException e){
            JOptionPane.showConfirmDialog(this, "Insira um número válido", "Warning", JOptionPane.ERROR_MESSAGE);
            TAMANHO = 30;
        }
    }//GEN-LAST:event_btnAlteraTamanhoActionPerformed

    private void btnSalvaImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvaImgActionPerformed
        try {
            ImageIO.write(Recurso.buffered, "jpg", new File("image.jpg"));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }//GEN-LAST:event_btnSalvaImgActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Configuracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Configuracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Configuracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Configuracoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Configuracoes().setVisible(true);
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlteraTamanho;
    private javax.swing.JButton btnColorChooser;
    private javax.swing.JButton btnSalvaImg;
    private javax.swing.ButtonGroup grupoOpcoes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JRadioButton rdBorracha;
    private javax.swing.JRadioButton rdBrush;
    private javax.swing.JRadioButton rdColorir;
    private javax.swing.JRadioButton rdEscrever;
    private javax.swing.JTextField txtTamanhoNovo;
    // End of variables declaration//GEN-END:variables
}
