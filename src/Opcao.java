/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Lucas
 */
public enum Opcao {
    BRUSH(1), BORRACHA(2), LINHA(3), COLORIR(4), ESCREVER(5), DEFAULT(6);
            
    private final int valor;
    
    Opcao(int valorOpcao){
        valor = valorOpcao;
    }
    public int getValor(){
        return valor;
    }        
}
