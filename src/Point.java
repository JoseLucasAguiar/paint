
import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jose Lucas
 */
public class Point {
    private int x;
    private int y;
    private int tamanho;
    private Color cor;

    public Point(int x, int y, int tamanho, Color cor) {
        this.x = x;
        this.y = y;
        this.tamanho = tamanho;
        this.cor = cor;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public Color getCor() {
        return cor;
    }

    public void setCor(Color cor) {
        this.cor = cor;
    }

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + ", tamanho=" + tamanho + ", cor=" + cor + '}';
    }
   
    
    
}
